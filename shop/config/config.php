<?php
return [
    // 应用Trace
    'app_trace' => true,
    // 入口自动绑定模块
    'auto_bind_module' => true,
    // 应用调试模式
    'app_debug' => true,
    // 是否自动转换URL中的控制器和操作名
    'url_convert' => false,
    // URL参数方式 0 按名称成对解析 1 按顺序解析
    'url_param_type' => 0,
    // URL普通方式参数 用于自动生成
    'url_common_param' => false,
    // 自动渲染模板
    'template' => [
        // 默认模板渲染规则 1 解析为小写+下划线 2 全部转换小写
        'auto_rule' => 2,
        // 配置模板全局配置 即所有方法都会用到同一个布局文件
        'layout_on' => true,
//        'layout_name' => 'layout',//默认就是layout 这里可以不写
//        'layout_item' => '{__CONTENT__}',//默认就是__CONTENT__ 除非修改 这里可以不写
    ],
    // 字符串替换
    'view_replace_str' => [
        '__JQUERY__' => '/static/jquery',
        '__IMAGE__' => '/static/image',
    ],
];