<?php

use think\Route;

Route::rule('classify/:type','index/index/classify');
Route::rule('search','index/index/search');
Route::rule('login','index/index/login');
Route::rule('logout','index/index/logout');
Route::rule('register','index/index/register');
Route::rule('myDetail','index/index/myDetail');
Route::rule('myOrder','index/index/myOrder');
Route::rule('purchase/:goodsId','index/index/purchase');
Route::rule('submitOrder','index/index/submitOrder');
Route::rule('pay','index/index/pay');
Route::rule('receive/:orderId','index/index/receive');