<?php

namespace app\Index\validate;

use think\Validate;

class payValidator extends Validate
{
    protected $rule = [
        'pay_password|支付密码'=>'require|length:6',
    ];

    protected $message = [
        'pay_password.require'=>'支付密码不能为空',
        'pay_password.length'=>'支付密码必须为6位',
    ];
}