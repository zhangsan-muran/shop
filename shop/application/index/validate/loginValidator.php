<?php

namespace app\index\validate;

use think\Validate;

class loginValidator extends Validate
{
    protected $rule = [
        'username|用户名'=>'require',
        'password|密码'=>'require',
    ];

    protected $message = [
        'username.require'=>'用户名不能为空',
        'password.require'=>'密码不能为空',
    ];
}