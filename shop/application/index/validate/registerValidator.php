<?php

namespace app\index\validate;

use think\Validate;

class registerValidator extends Validate
{
    protected $rule=[
        'username|用户名'=>'require|length:2,10',
        'password|密码'=>'require|length:6,16',
        'repassword|重复密码'=>'require|confirm:password',
        'phone|手机号'=>'require|number|length:11',
        'address|收货地址'=>'require',
        'balance|银行卡余额'=>'require|number|length:1,8|>=:0',
        'pay_password|支付密码'=>'require|number|length:6',
    ];

    protected $message=[
        'username.require'=>'用户名不能为空',
        'username.length'=>'用户名长度必须为2-10',
        'password.require'=>'密码不能为空',
        'password.length'=>'密码长度必须为6-16',
        'repassword.require'=>'重复密码不能为空',
        'repassword.confirm'=>'重复密码与密码不一致',
        'phone.require'=>'手机号不能为空',
        'phone.number'=>'手机号必须为数字',
        'phone.length'=>'手机号长度必须为11',
        'address.require'=>'收货地址不能为空',
        'balance.require'=>'银行卡余额不能为空',
        'balance.number'=>'银行卡余额必须为数字',
        'balance.length'=>'银行卡余额长度必须为1-8',
        'balance.egt'=>'银行卡余额不能为负数', // 这里不能用balance.>=来表示
        'pay_password.require'=>'支付密码不能为空',
        'pay_password.number'=>'支付密码必须为数字',
        'pay_password.length'=>'支付密码必须为6位',
    ];
}